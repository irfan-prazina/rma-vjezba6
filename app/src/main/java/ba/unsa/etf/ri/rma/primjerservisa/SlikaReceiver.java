package ba.unsa.etf.ri.rma.primjerservisa;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

public class SlikaReceiver extends BroadcastReceiver {
    AktivnostSlikaInterface aktivnost;

    public SlikaReceiver(AktivnostSlikaInterface aktivnost) {
        this.aktivnost = aktivnost;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction()==context.getString(R.string.akcija_pokret)) {
            Bitmap bmp = (Bitmap) intent.getParcelableExtra("slika");
            aktivnost.postaviSliku(bmp);
            aktivnost.posaljiNotifikaciju();
        }
    }
}
