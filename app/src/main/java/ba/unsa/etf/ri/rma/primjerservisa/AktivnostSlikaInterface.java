package ba.unsa.etf.ri.rma.primjerservisa;

import android.graphics.Bitmap;

public interface AktivnostSlikaInterface {
    void postaviSliku(Bitmap bmp);
    void posaljiNotifikaciju();
}
