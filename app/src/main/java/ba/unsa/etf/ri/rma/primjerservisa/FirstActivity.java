package ba.unsa.etf.ri.rma.primjerservisa;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class FirstActivity extends AppCompatActivity implements AktivnostSlikaInterface {
    String AKCIJA_POKRET;
    Button dugmeV;
    ImageView imageV;
    Boolean pokreni = true;
    public String ID_KANALA = "kameraKanal";
    BroadcastReceiver slikaReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        AKCIJA_POKRET=getString(R.string.akcija_pokret);
        dugmeV = (Button) findViewById(R.id.dugme);
        imageV = (ImageView) findViewById(R.id.slika);

        cameraPermission();
        createNotificationChannel();

        Intent pocetniIntent = getIntent();
        Bitmap bmp = (Bitmap) pocetniIntent.getParcelableExtra("slika");
        if (pocetniIntent != null && bmp != null) {
            imageV.setImageBitmap(bmp);
            pokreni = false;
            dugmeV.setText("STOP");
        }

        dugmeV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pokreni) {
                    Intent intentPokretanje = new Intent(getApplicationContext(), KameraServis.class);
                    startService(intentPokretanje);
                    dugmeV.setText("STOP");
                } else {
                    stopService(new Intent(getApplicationContext(), KameraServis.class));
                    dugmeV.setText("START");
                }
                pokreni = !pokreni;
            }
        });

        slikaReceiver = new SlikaReceiver(this);
        IntentFilter filter = new IntentFilter(AKCIJA_POKRET);
        registerReceiver(slikaReceiver,filter);
    }

    @Override
    protected void onDestroy() {
        stopService(new Intent(FirstActivity.this, KameraServis.class));
        unregisterReceiver(slikaReceiver);
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent startIntent = getIntent();
        Bitmap bmp = (Bitmap) startIntent.getParcelableExtra("slika");
        if (bmp != null) {
            pokreni = false;
            dugmeV.setText("STOP");
        }
    }

    public void cameraPermission() {
        if (ContextCompat.checkSelfPermission(FirstActivity.this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(FirstActivity.this,
                    Manifest.permission.CAMERA)) {
            } else {
                ActivityCompat.requestPermissions(FirstActivity.this,
                        new String[]{Manifest.permission.CAMERA},
                        1);
            }
        }
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "kanalKamera";
            String description = "Kanal poruka za notifikacije iz kamera servisa.";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(ID_KANALA, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public void postaviSliku(Bitmap bmp) {

        int nh = (int) (bmp.getHeight() * (1024.0 / bmp.getWidth()));
        Bitmap skalirana = Bitmap.createScaledBitmap(bmp, 1024, nh, true);
        imageV.setImageBitmap(skalirana);
    }

    @Override
    public void posaljiNotifikaciju() {
        Notification.Builder note;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            note = new Notification.Builder(getApplicationContext(), ID_KANALA);
        } else {
            note = new Notification.Builder(getApplicationContext());
        }
        note.setCategory(Notification.CATEGORY_MESSAGE);
        note.setContentTitle("Serivs");
        note.setContentText("Stigla je nova slika");
        note.setSmallIcon(android.R.drawable.ic_menu_camera);
        note.setAutoCancel(true);
        note.setVisibility(Notification.VISIBILITY_PUBLIC);
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(1, note.build());
    }
}
