package ba.unsa.etf.ri.rma.primjerservisa.utility;

import android.graphics.Bitmap;

/**
 * Created by irfan on 07.04.2018..
 */

public class BitmapUtilities {

    public static int pixelDiff(int rgb1, int rgb2) {
        int r1 = (rgb1 >> 16) & 0xff;
        int r2 = (rgb2 >> 16) & 0xff;
        return Math.abs(r1 - r2);
    }

    public static float razlikaBitmapa(Bitmap b1, Bitmap b2) {
        int[] pikseli1 = new int[b1.getWidth() * b1.getHeight()];
        int[] pikseli2 = new int[b2.getWidth() * b2.getHeight()];
        b1.getPixels(pikseli1, 0, b1.getWidth(), 0, 0, b1.getWidth(), b1.getHeight());
        b2.getPixels(pikseli2, 0, b2.getWidth(), 0, 0, b2.getWidth(), b2.getHeight());

        if (pikseli1.length != pikseli2.length) return 1;
        long razlika = 0;
        for (int i = 0; i < pikseli1.length; i++) {
            razlika += pixelDiff(pikseli1[i], pikseli2[i]);
        }
        return (float) razlika / (float) (255 * pikseli1.length);
    }
}
