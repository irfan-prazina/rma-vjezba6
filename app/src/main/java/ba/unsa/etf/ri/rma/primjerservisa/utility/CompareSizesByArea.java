package ba.unsa.etf.ri.rma.primjerservisa.utility;

import android.util.Size;

import java.util.Comparator;

/**
 * Created by irfan on 07.04.2018..
 */
class CompareSizesByArea implements Comparator<Size> {

    @Override
    public int compare(Size lhs, Size rhs) {
        return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                (long) rhs.getWidth() * rhs.getHeight());
    }

}
