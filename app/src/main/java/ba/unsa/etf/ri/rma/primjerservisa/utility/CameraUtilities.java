package ba.unsa.etf.ri.rma.primjerservisa.utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Size;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by irfan on 4/5/18.
 */

public class CameraUtilities {

    private CameraManager cmanager;
    private CameraCaptureSession ccsession;
    private ImageReader ireader;
    private HandlerThread hthread;
    private Handler chandler;
    private String idKamere;
    private Size velicina;
    private Context ctx;
    private Callbacks pozivaoc;
    private CameraDevice cDevice;

    public CameraUtilities(Context ctx, CameraUtilities.Callbacks pozivaoc) throws CameraAccessException, SecurityException {
        this.hthread = new HandlerThread("threadSlike");
        this.hthread.start();
        this.chandler = new Handler(hthread.getLooper());
        this.cmanager = (CameraManager) ctx.getSystemService(Context.CAMERA_SERVICE);
        this.idKamere = cmanager.getCameraIdList()[0];
        postaviImageReader();
        this.pozivaoc = pozivaoc;
        this.ctx = ctx;
    }

    private void kreirajSesiju() throws CameraAccessException {
        cDevice.createCaptureSession(Arrays.asList(ireader.getSurface()), new CameraCaptureSession.StateCallback() {
            @Override
            public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                ccsession = cameraCaptureSession;
                try {
                    slikajSada();
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }
                Log.i("CameraUtilities", "Sesija kreirana!");
            }

            @Override
            public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                Log.e("CameraUtilities", "Sesija nije kreirana!");
            }
        }, chandler);
    }


    public void zaustaviThreadSlike() {
        if (hthread != null) {
            hthread.quitSafely();
            try {
                hthread.join();
                hthread = null;
                chandler.removeCallbacksAndMessages(null);
                chandler = null;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void slikajSada() throws CameraAccessException {
        CaptureRequest.Builder crb = cDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
        crb.addTarget(ireader.getSurface());

        ccsession.abortCaptures();
        ccsession.capture(crb.build(), new CameraCaptureSession.CaptureCallback() {
            @Override
            public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                super.onCaptureCompleted(session, request, result);
                Log.i("CameraUtilities", "Uslikana slika!");
            }
        }, chandler);
    }

    public void uslikaj() throws SecurityException, CameraAccessException {
        if (ccsession == null) {
            cmanager.openCamera(idKamere, new CameraDevice.StateCallback() {
                @Override
                public void onOpened(@NonNull CameraDevice cameraDevice) {
                    try {
                        cDevice = cameraDevice;
                        kreirajSesiju();
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onDisconnected(@NonNull CameraDevice cameraDevice) {
                }

                @Override
                public void onError(@NonNull CameraDevice cameraDevice, int i) {
                    Log.e("GREŠKA PRI openCamera:", Integer.toString(i));
                }
            }, chandler);
        } else {
            slikajSada();
        }
    }

    private void postaviImageReader() throws CameraAccessException {
        CameraCharacteristics characteristics = cmanager.getCameraCharacteristics(idKamere);
        StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

        velicina = Collections.min(
                Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)),
                new CompareSizesByArea());
        ireader = ImageReader.newInstance(velicina.getWidth(), velicina.getHeight(), ImageFormat.JPEG, 2);
        ireader.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener() {
            @Override
            public void onImageAvailable(ImageReader imageReader) {
                Image slika = imageReader.acquireLatestImage();
                pozivaoc.vratiBmp(dajBitmap(slika));
            }
        }, chandler);
    }


    private Bitmap dajBitmap(Image slika) {

        ByteBuffer buffer = slika.getPlanes()[0].getBuffer();
        byte[] bytes = new byte[buffer.capacity()];
        buffer.get(bytes);

        Bitmap original = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

        int nh = (int) (original.getHeight() * (256.0 / original.getWidth()));
        Bitmap skalirana = Bitmap.createScaledBitmap(original, 256, nh, true);

        slika.close();
        return skalirana;
    }

    public interface Callbacks {
        void vratiBmp(Bitmap bmp);
    }


}
