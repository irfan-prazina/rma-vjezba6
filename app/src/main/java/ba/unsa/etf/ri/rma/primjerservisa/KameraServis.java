package ba.unsa.etf.ri.rma.primjerservisa;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.camera2.CameraAccessException;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.TypedArrayUtils;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Timer;

import ba.unsa.etf.ri.rma.primjerservisa.utility.BitmapUtilities;
import ba.unsa.etf.ri.rma.primjerservisa.utility.CameraUtilities;

public class KameraServis extends Service implements CameraUtilities.Callbacks {
    private Timer t;
    boolean vratio = false;
    Bitmap prosla = null;
    protected CameraUtilities cu;


    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (t == null) {
            try {
                cu = new CameraUtilities(getApplicationContext(), KameraServis.this);
                t = new Timer();
                t.schedule(new SlikanjeTask(cu), 0, 2000);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        t.cancel();

        cu.zaustaviThreadSlike();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void vratiBmp(Bitmap bmp) {
        float ra = BitmapUtilities.razlikaBitmapa((prosla != null) ? prosla : bmp, bmp);
        Log.i("RAZLIKA", Float.toString(ra));
        if (prosla != null && ra > 0.1) {
            Intent intent = new Intent(getString(R.string.akcija_pokret));
            intent.putExtra("slika", bmp);
            sendBroadcast(intent);
        }
        prosla = bmp;
    }


}
