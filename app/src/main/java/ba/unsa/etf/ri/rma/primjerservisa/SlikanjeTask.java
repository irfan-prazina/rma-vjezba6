package ba.unsa.etf.ri.rma.primjerservisa;

import android.hardware.camera2.CameraAccessException;

import java.util.TimerTask;

import ba.unsa.etf.ri.rma.primjerservisa.utility.CameraUtilities;

/**
 * Created by irfan on 07.04.2018..
 */
class SlikanjeTask extends TimerTask {
    CameraUtilities mojCU;

    public SlikanjeTask(CameraUtilities mojCU) {
        this.mojCU = mojCU;
    }

    @Override
    public void run() {
        try {
            mojCU.uslikaj();
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

}
